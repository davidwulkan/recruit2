@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create Interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">Interview Description</label>
            <input type = "text" class="form-control" name = "name">
        </div>     
        
        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
