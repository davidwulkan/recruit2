@extends('layouts.app')

@section('title', 'interviews')

@section('content')

<a href="{{url('/candidates/createinterview')}}"  class="btn btn-primary" >Add new Interview</a>

<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>text</th><th>Candidate</th><td>User</td><th>Created at</th><th>Updated at</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>       
            <td>{{$interview->id}}</td>
            <td>{{$interview->text}}</td>
            <td>
            <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(isset($interview->candidate_id))
                          {{$interview->candidate->name}}  
                        @else
                            Assign Candidate
                        @endif

                        
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($candidates as $candidate)
                    <a class="dropdown-item" href="{{route('candidates.changecandidate',[$interview->id,$candidate->id])}}">{{$candidate->name}}</a>
                    @endforeach

            </td>
            <td>
            <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(isset($interview->user_id))
                          {{$interview->user->name}}  
                        @else
                          Assign user
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($users as $user)
                    <a class="dropdown-item" href="{{route('candidates.changeuserint',[$interview->id,$user->id])}}">{{$user->name}}</a>
                    @endforeach
                    </div>
                  </div>

            </td>
            
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
                                                                                                            
        </tr>
    @endforeach
</table>
@endsection